const mongoose = require("mongoose");

const productSchema = mongoose.Schema({
  productName: { type: String, required: true },
  category: { type: String, required: true },
  gender: { type: String, required: true },
  images: { type: Array, required: true },
  length: { type: Number, required: false },
  weight: { type: Number, required: false },
  price: { type: Number, required: true },
  stock: { type: Number, required: true },
});

module.exports = mongoose.model("Product", productSchema);
