export interface Employee {
  id: string;
  fName: string;
  lName: string;
  position: string;
  image: string;
  description: string;
  instagram: string;
  facebook: string;
}
