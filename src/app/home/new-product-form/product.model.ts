export interface Product {
  id: string;
  productName: string;
  category: string;
  gender: string;
  images: Array<string>;
  length: number;
  weight: number;
  price: number;
  stock: number;
}
