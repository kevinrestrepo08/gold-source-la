import {
  Component,
  ElementRef,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { mimeType } from 'src/app/about-us/mime-type.validator';
import { PostsService } from 'src/app/posts.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subscription } from 'rxjs/internal/Subscription';
import { ActivatedRoute } from '@angular/router';
import { Product } from './product.model';

@Component({
  selector: 'app-new-product-form',
  templateUrl: './new-product-form.component.html',
  styleUrls: ['./new-product-form.component.scss'],
})
export class NewProductFormComponent implements OnInit {
  @ViewChild('imagesPreview') div: ElementRef;
  private subs: Subscription;
  private productId: string;
  product: Product;
  selectedImages: any[] = [];
  previewImages: any[] = [];
  newProductForm: FormGroup;
  disabledButton = false;
  imagePreview: string;
  mode: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { mode: string },
    public postService: PostsService,
    private snackBar: MatSnackBar,
    public route: ActivatedRoute,
    public dialogRef: MatDialogRef<NewProductFormComponent>
  ) {}

  ngOnInit() {
    this.initForm();
    this.subs = this.route.queryParamMap.subscribe((params) => {
      params.has('productId')
        ? ((this.mode = 'edit'),
          (this.productId = params.get('productId')),
          this.postService
            .getProduct(this.productId)
            .subscribe((productData) => {
              this.product = {
                id: productData._id,
                productName: productData.productName,
                category: productData.category,
                gender: productData.gender,
                length: productData.length,
                weight: productData.weight,
                images: productData.images,
                price: productData.price,
                stock: productData.stock,
              };
              this.newProductForm.setValue({
                productName: this.product.productName,
                category: this.product.category,
                gender: this.product.gender,
                length: this.product.length,
                weight: this.product.weight,
                images: this.product.images,
                price: this.product.price,
                stock: this.product.stock,
              });
            }))
        : (this.mode = 'create');
    });
  }

  initForm() {
    this.newProductForm = new FormGroup({
      productName: new FormControl(null, {
        validators: [Validators.required],
      }),
      category: new FormControl(null, {
        validators: [Validators.required],
      }),
      gender: new FormControl(null, { validators: [Validators.required] }),
      length: new FormControl(null),
      weight: new FormControl(null),
      price: new FormControl(null, { validators: [Validators.required] }),
      stock: new FormControl(null, { validators: [Validators.required] }),
      // description: new FormControl(null, { validators: [Validators.required] }),
      images: new FormControl(this.selectedImages),
    });
  }

  async onSubmitProduct(form: FormGroup) {
    this.disabledButton = true;
    let response;
    switch (this.mode) {
      case 'create':
        this.snackBar.open('Adding product to database 👨🏽‍💻');
        response = await this.postService.createProduct(form.value);
        if (response) {
          this.snackBar.open('Finished adding product to database 👍🏽', '', {
            duration: 3000,
          });
        }
        break;
      case 'edit':
        this.snackBar.open('Editing product in database 👨🏽‍💻');
        response = await this.postService.updateProduct(
          this.productId,
          form.value
        );
        if (response) {
          this.snackBar.open('Finished editing product in database 👍🏽', '', {
            duration: 3000,
          });
        }
        break;
    }
    //resturn the result and the form to the home component
    this.dialogRef.close({ result: response, form: form });
  }

  onImagePicked(event: Event) {
    const files = (event.target as HTMLInputElement).files;
    for (let i = 0; i < files.length; i++) {
      this.selectedImages.push(files[i]);
      const reader = new FileReader();
      reader.onload = () => {
        this.imagePreview = reader.result as string;
        this.previewImages.push(this.imagePreview);
      };
      reader.readAsDataURL(files[i]);
    }
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}
