const express = require("express");
const multer = require("multer");

const checkAuth = require("../middleware/check-auth");

const Products = require("../models/productModel");

const router = express.Router();

// include node fs module
const fs = require("fs");
const path = require("path");

const mime_type_map = {
  "image/png": "png",
  "image/jpeg": "jpg",
  "image/jpg": "jpg",
};
// const storage = multer.diskStorage({
//   destination: (req, file, cb) => {
//     const isValid = mime_type_map[file.mimetype];
//     let error = new Error("invalid mime type");
//     if (isValid) {
//       error = null;
//     }
//     console.log("in multer test");
//     cb(null, "backend/images");
//   },
//   filename: (req, file, cb) => {
//     const name = file.originalname.toLocaleLowerCase().split(" ").join("-");
//     const ext = mime_type_map[file.mimetype];
//     cb(null, name + "-" + Date.now() + "." + ext);
//   },
// });

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "backend/images/products");
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + file.originalname);
  },
});

router.post(
  "/addNewProduct",
  multer({ storage: storage }).array("images", 12),
  (req, res, next) => {
    const url = req.protocol + "://" + req.get("host");
    let uploadedImages = [];
    req.files.forEach((image) => {
      uploadedImages.push(url + "/images/products/" + image.filename);
    });

    const products = new Products({
      productName: req.body.productName,
      category: req.body.category,
      gender: req.body.gender,
      weight: req.body.weight,
      length: req.body.length,
      price: req.body.price,
      stock: req.body.stock,
      weight: req.body.weight,
      images: uploadedImages,
    });

    products
      .save()
      .then((addedProduct) => {
        res.status(201).json({
          message: "product added sucessfully!",
          product: {
            ...addedProduct,
            id: addedProduct._id,
          },
          images: uploadedImages,
        });
      })
      .catch((error) => {
        res.status(500).json({
          message: "Adding product failed!",
        });
      });
  }
);

router.put(
  "/:id",
  checkAuth,
  multer({ storage: storage }).array("images", 12),
  (req, res, next) => {
    let imagePaths = req.body.images.split(",");
    // if (req.file) {
    //   const url = req.protocol + "://" + req.get("host");
    //   imagePath = url + "/images/products" + req.file.filename;
    //   //if user is uploading a new img file, then delete the old one from the project storage
    //   //let previousImg;
    //   // Products.findById(req.params.id).then((product) => {
    //   //   previousImg = product.images.replace(
    //   //     "http://localhost:3000/images",
    //   //     "backend/images"
    //   //   );
    //   //   try {
    //   //     fs.unlink(previousImg, (err) => {
    //   //       if (err) {
    //   //         console.log("error in update route", err);
    //   //       } else {
    //   //         console.log("file deleted");
    //   //       }
    //   //     });
    //   //   } catch (error) {}
    //   // });
    // }
    const product = new Products({
      _id: req.body.id,
      productName: req.body.productName,
      gender: req.body.gender,
      category: req.body.category,
      images: imagePaths,
      length: req.body.length,
      weight: req.body.weight,
      price: req.body.price,
      stock: req.body.stock,
    });

    Products.updateOne({ _id: req.params.id }, product)
      .then((result) => {
        if (result.modifiedCount > 0) {
          res.status(200).json({
            message: "Update successful!",
            product: {
              ...product,
              id: product._id,
              images: product.images,
            },
          });
        } else {
          res.status(401).json({ message: "Not authorized!" });
        }
      })
      .catch((error) => {
        res.status(500).json({
          message: "Couldn't udpate product!",
        });
      });
  }
);

router.get("", (req, res, next) => {
  const productsQuery = Products.find();
  let fetchedProducts;
  productsQuery
    .then((documents) => {
      fetchedProducts = documents;
    })
    .then(() => {
      res.status(200).json({
        message: "Products fetched successfully!",
        products: fetchedProducts,
      });
    })
    .catch((error) => {
      res.status(500).json({
        message: "fetching products failed",
      });
    });
});

router.get("/:id", (req, res, next) => {
  Products.findById(req.params.id)
    .then((product) => {
      if (product) {
        res.status(200).json(product);
      } else {
        res.status(400).json({ message: "Product not found" });
      }
    })
    .catch((error) => {
      res.status(500).json({
        message: "Fetching product failed!",
      });
    });
});

router.delete("/:id/:images", checkAuth, (req, res, next) => {
  let images = req.params.images.split(",");

  Products.deleteOne({ _id: req.params.id })
    .then((result) => {
      if (result.deletedCount > 0) {
        images.forEach((element) => {
          fs.unlink("backend/images/products/" + element, (err) => {
            if (err) {
              console.log("error in deleting img", err);
            } else {
              console.log("img deleted");
            }
          });
        });
        res.status(200).json({ message: "product deleted successfully" });
      } else {
        res.status(401).json({ message: "not authorized" });
      }
    })
    .catch((error) => {
      res.status(500).json({
        message: "Deleting product failed!",
      });
    });
});

module.exports = router;
