import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { PostsService } from '../posts.service';
import { NewProductFormComponent } from './new-product-form/new-product-form.component';
import { Product } from './new-product-form/product.model';
import { MatSnackBar } from '@angular/material/snack-bar';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { Router } from '@angular/router';

const RULER_ICON = `<?xml version="1.0" encoding="iso-8859-1"?>
<!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
	<g>
		<g>
			<path d="M418.364,0L0,418.364L93.636,512L512,93.636L418.364,0z M22.153,418.364l16.443-16.443l54.875,54.875l11.076-11.076
			l-54.875-54.875l10.939-10.939l16.477,16.477l11.076-11.077l-16.477-16.477l10.939-10.939l27.485,27.485l11.077-11.076
			l-27.485-27.485l10.939-10.939l16.477,16.477l11.076-11.077l-16.477-16.477l10.939-10.939l54.874,54.875l11.076-11.077
			l-54.875-54.875l10.939-10.939l16.477,16.477l11.076-11.076l-16.476-16.474l10.939-10.939l27.485,27.485l11.076-11.076
			l-27.485-27.485l10.939-10.939l16.477,16.477l11.076-11.076l-16.477-16.477l10.939-10.939l54.874,54.875l11.077-11.076
			l-54.874-54.875l10.939-10.939l16.477,16.477l11.076-11.076l-16.477-16.477l10.939-10.939l27.485,27.485l11.076-11.076
			L269.83,170.69l10.939-10.939l16.477,16.477l11.077-11.076l-16.477-16.477l10.939-10.939l54.875,54.875l11.077-11.076
			l-54.875-54.874l10.939-10.939l16.477,16.477l11.077-11.076l-16.477-16.477l10.939-10.939l27.485,27.485l11.076-11.076
			l-27.489-27.487l10.939-10.939l16.477,16.477l11.077-11.076l-16.478-16.477l10.939-10.939l54.875,54.875l11.076-11.076
			l-54.875-54.875l16.443-16.443l71.482,71.482L93.636,489.847L22.153,418.364z" />
		</g>
	</g>
	<g></g>
	<g></g>
	<g></g>
	<g></g>
	<g></g>
	<g></g>
	<g></g>
	<g></g>
	<g></g>
	<g></g>
	<g></g>
	<g></g>
	<g></g>
	<g></g>
	<g></g>
</svg>`;

const SCALE_ICON = `<?xml version='1.0' encoding='iso-8859-1'?>
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 310.709 310.709" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 310.709 310.709">
  <g>
    <path d="m287.365,7.878c-4.252-5.007-10.461-7.878-17.034-7.878h-229.952c-6.565,0-12.771,2.872-17.027,7.882-4.261,5.018-6.09,11.613-5.02,18.096 3.095,18.719 12.746,36.591 27.911,51.682 7.964,7.924 18.536,12.288 29.771,12.288h158.683c11.234,0 21.807-4.364 29.771-12.288 15.165-15.092 24.816-32.963 27.909-51.681 1.072-6.488-0.753-13.084-5.012-18.101zm-40.533,52.06c-3.246,3.229-7.556,5.009-12.136,5.009h-158.683c-4.58,0-8.89-1.779-12.136-5.009-10.55-10.499-17.515-22.518-20.279-34.938h223.512c-2.764,12.42-9.728,24.439-20.278,34.938z" />
    <path d="m243.145,110.128h-175.581c-6.903,0-12.5,5.597-12.5,12.5v175.581c0,6.903 5.597,12.5 12.5,12.5h175.581c6.903,0 12.5-5.597 12.5-12.5v-175.581c2.84217e-14-6.903-5.597-12.5-12.5-12.5zm-12.5,175.581h-150.581v-150.581h150.581v150.581z" />
    <path d="m155.355,272.659c34.32,0 62.241-27.921 62.241-62.241s-27.921-62.241-62.241-62.241-62.241,27.921-62.241,62.241 27.921,62.241 62.241,62.241zm0-99.482c1.578,0 3.129,0.11 4.656,0.301l-15.813,31.304c-3.112,6.162-0.641,13.681 5.521,16.793 1.807,0.913 3.73,1.346 5.626,1.346 4.566,0 8.968-2.512 11.167-6.867l15.804-31.286c6.362,6.684 10.28,15.715 10.28,25.65 0,20.535-16.706,37.241-37.241,37.241s-37.241-16.706-37.241-37.241 16.706-37.241 37.241-37.241z" />
  </g>
</svg>`;

const INVENTORY_ICON = `<?xml version="1.0" encoding="iso-8859-1"?>
<!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 484.185 484.185" style="enable-background:new 0 0 484.185 484.185;" xml:space="preserve">
	<g>
		<g>
			<g>
				<path d="M429.255,0v149.74h-39.904V69.878H239.61v-39.93H84.854V149.74H49.94V0H0.001v484.185h49.938v-89.844h379.315v89.844
				h54.931V0H429.255z M15.001,469.185V15h19.938v454.185H15.001z M239.61,84.878h134.74v64.861H239.61V84.878z M149.766,44.948
				h24.931v44.896h-24.931V44.948z M99.854,44.948h34.913v59.896h54.931V44.948h34.913V149.74H99.854V44.948z M429.255,379.341
				H49.94v-14.948h379.315V379.341z M109.836,349.393v-64.861h134.74v64.861H109.836z M349.419,304.497v-59.896h34.913v104.792
				H259.576V244.601h34.913v59.896H349.419z M309.49,289.497v-44.896h24.931v44.896H309.49z M429.255,349.393h-29.922V229.601
				H244.576v39.931H94.836v79.861H49.94V194.688h379.315V349.393z M429.255,179.688H49.94V164.74h379.315V179.688z M469.185,469.185
				h-24.931V15h24.931V469.185z" />
				<rect x="326.945" y="109.809" width="29.948" height="15" />
				<rect x="267.049" y="109.809" width="49.913" height="15" />
				<rect x="197.17" y="309.462" width="29.948" height="15" />
				<rect x="137.276" y="309.462" width="49.913" height="15" />
			</g>
		</g>
	</g>
	<g></g>
	<g></g>
	<g></g>
	<g></g>
	<g></g>
	<g></g>
	<g></g>
	<g></g>
	<g></g>
	<g></g>
	<g></g>
	<g></g>
	<g></g>
	<g></g>
	<g></g>
</svg>`;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [
    // the fade-in/fade-out animation.
    trigger('FadeAnimation', [
      // the "in" style determines the "resting" state of the element when it is visible.
      state('in', style({ opacity: 1 })),

      // fade in when created. this could also be written as transition('void => *')
      transition(':enter', [style({ opacity: 0 }), animate(200)]),

      // fade out when destroyed. this could also be written as transition('void => *')
      transition(':leave', animate(400, style({ opacity: 0 }))),
    ]),
  ],
})
export class HomeComponent implements OnInit {
  userIsAuthenticated = false;
  loading = true;
  private authStatusSub: Subscription;

  products: Product[];
  mProducts: Product[];
  wProducts: Product[];
  productsSub: Subscription;
  mCategories: any[];
  wCategories: any[];
  menSearchFilters = new Array();
  womanSearchFilters = new Array();

  constructor(
    private authService: AuthService,
    private postService: PostsService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private router: Router,
    matIconRegistry: MatIconRegistry,
    domSanitizer: DomSanitizer
  ) {
    matIconRegistry.addSvgIconLiteral(
      'ruler-icon',
      domSanitizer.bypassSecurityTrustHtml(RULER_ICON)
    );

    matIconRegistry.addSvgIconLiteral(
      'scale-icon',
      domSanitizer.bypassSecurityTrustHtml(SCALE_ICON)
    );

    matIconRegistry.addSvgIconLiteral(
      'inventory-icon',
      domSanitizer.bypassSecurityTrustHtml(INVENTORY_ICON)
    );
  }

  ngOnInit() {
    this.loading = true;
    this.checkAuth();
    this.postService.getAllProducts();
    this.productsSub = this.postService
      .getAllProductsUpdateListener()
      .subscribe((productData: { products: Product[] }) => {
        this.products = productData.products;
        this.filterProducts();
        this.loading = false;
      });
  }

  selectMensChip(event: any) {
    console.log('event', event);
    let selectedChip = document.getElementById(event.target.id);
    let filterString = event.target.innerText;
    if (selectedChip.classList.contains('mat-chip-selected')) {
      selectedChip.classList.remove('mat-chip-selected');
      //remove category to filter to search filters array
      let index = this.menSearchFilters.indexOf(filterString);
      this.menSearchFilters.splice(index, 1);
      console.log('search filters', this.menSearchFilters);
    } else {
      selectedChip.classList.add('mat-chip-selected');
      //add category to filter to search filters array
      this.menSearchFilters.push(filterString);
      console.log('search filters', this.menSearchFilters);
    }
    //filter products acording to chip sleection
    this.mProducts = this.products.filter(
      (element) =>
        this.menSearchFilters.includes(element.category) &&
        element.gender == 'Men'
    );
    if (this.menSearchFilters.length == 0) {
      this.filterProducts();
    }
  }
  selectWomansChip(event: any) {
    console.log('event', event);
    let selectedChip = document.getElementById(event.target.id);
    let filterString = event.target.innerText;
    if (selectedChip.classList.contains('mat-chip-selected')) {
      selectedChip.classList.remove('mat-chip-selected');
      //remove category to filter to search filters array
      let index = this.womanSearchFilters.indexOf(filterString);
      this.womanSearchFilters.splice(index, 1);
      console.log('search filters', this.womanSearchFilters);
    } else {
      selectedChip.classList.add('mat-chip-selected');
      //add category to filter to search filters array
      this.womanSearchFilters.push(filterString);
      console.log('search filters', this.womanSearchFilters);
    }
    //filter products acording to chip sleection
    this.wProducts = this.products.filter(
      (element) =>
        this.womanSearchFilters.includes(element.category) &&
        element.gender == 'Women'
    );
    if (this.womanSearchFilters.length == 0) {
      this.filterProducts();
    }
  }

  openProductForm() {
    const dialogRef = this.dialog.open(NewProductFormComponent);

    dialogRef.afterClosed().subscribe((result) => {
      this.router.navigate(['/home'], {
        queryParams: {
          productId: null,
        },
        queryParamsHandling: 'merge',
      });

      if (result) {
        let newProduct = {
          id: result.result.message.product.id,
          ...result.form.value,
        };
        if (result.result.message.message == 'Update successful!') {
          newProduct.images = result.result.message.product.images;
          let index;
          for (let i = 0; i < this.products.length; i++) {
            if (this.products[i].id == newProduct.id) {
              index = i;
              this.products[index] = newProduct;
            }
          }
        } else if (
          result.result.message.message == 'product added sucessfully!'
        ) {
          newProduct.images = result.result.message.images;
          this.products.unshift(newProduct);
        }
      }
      this.filterProducts();
    });
  }

  getClassOfStock(amount: number) {
    let className = '';
    amount > 3
      ? (className = 'badge rounded-pill bg-success')
      : amount > 0 && amount < 3
      ? (className = 'badge rounded-pill bg-warning')
      : (className = 'badge rounded-pill bg-danger');
    return className;
  }
  getAmount(amount: number) {
    let message = '';
    amount > 0
      ? (message = 'In stock: ' + amount.toString() + ' left 😉')
      : (message = 'Out of stock 😔');
    return message;
  }
  checkAuth() {
    this.userIsAuthenticated = this.authService.getIsAuth();
    this.authStatusSub = this.authService
      .getAuthStatusListener()
      .subscribe((isAuthenticated) => {
        this.userIsAuthenticated = isAuthenticated;
        // this.userId = this.authService.getUserId();
      });
  }

  onDelete(product: Product) {
    this.snackBar.open('Trying to delete ' + product.productName);
    this.postService.deleteProduct(product).subscribe(() => {
      let index = this.products.findIndex(
        (producto) => producto.id == product.id
      );

      console.log('product', product);

      if (index > -1) {
        this.products.splice(index, 1);
        this.snackBar.open('deletion successful! 👍🏽', '', {
          duration: 3000,
        });
        this.filterProducts();
      } else {
        this.snackBar.open('something went wrong!' + '🤷🏽', '', {
          duration: 3000,
        });
      }
    });
  }

  filterProducts() {
    this.mProducts = this.products.filter((product) => product.gender == 'Men');
    this.wProducts = this.products.filter(
      (product) => product.gender == 'Women'
    );
    this.mCategories = [
      ...new Set(
        this.mProducts.map((element) => {
          return element.category;
        })
      ),
    ];
    this.wCategories = [
      ...new Set(
        this.wProducts.map((element) => {
          return element.category;
        })
      ),
    ];
    console.log('this.mCategories', this.mCategories);
    console.log('this.wCategories', this.wCategories);
  }

  addToShoppingCart(product: Product) {
    this.postService.addToShoppingCart(product);
  }

  ngOnDestroy() {
    this.authStatusSub.unsubscribe();
    this.productsSub.unsubscribe();
  }
}
