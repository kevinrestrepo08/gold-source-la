//used to retrieve single user from db
const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");

const userSchema = mongoose.Schema({
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
});

//mongoose plugin to check if email is unique in mongodb
userSchema.plugin(uniqueValidator);

module.exports = mongoose.model("Admin", userSchema);
