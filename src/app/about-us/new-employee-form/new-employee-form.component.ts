import { Component, OnInit, Inject, HostListener } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PostsService } from 'src/app/posts.service';
import { mimeType } from '../mime-type.validator';
import { ActivatedRoute } from '@angular/router';
import { Employee } from '../employee.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-new-employee-form',
  templateUrl: './new-employee-form.component.html',
  styleUrls: ['./new-employee-form.component.scss'],
})
export class NewEmployeeFormComponent implements OnInit {
  private empId: string;
  private subs: Subscription;
  newEmployeeForm: FormGroup;
  imagePreview: string;
  employee: Employee;
  mode: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { mode: string },
    public postService: PostsService,
    public route: ActivatedRoute,
    public dialogRef: MatDialogRef<NewEmployeeFormComponent>,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.newEmployeeForm = new FormGroup({
      fName: new FormControl(null, { validators: [Validators.required] }),
      lName: new FormControl(null, { validators: [Validators.required] }),
      position: new FormControl(null, { validators: [Validators.required] }),
      image: new FormControl(null, {
        validators: [Validators.required],
        asyncValidators: [mimeType],
      }),
      description: new FormControl(null, { validators: [Validators.required] }),
      instagram: new FormControl(null, [
        Validators.pattern('https://www.instagram.com/(.+)'),
      ]),
      facebook: new FormControl(null, [
        Validators.pattern('https://www.facebook.com/(.+)'),
      ]),
    });
    this.subs = this.route.queryParamMap.subscribe((params) => {
      params.has('empId')
        ? ((this.mode = 'edit'),
          (this.empId = params.get('empId')),
          this.postService.getEmployee(this.empId).subscribe((employeeData) => {
            this.employee = {
              id: employeeData._id,
              fName: employeeData.fName,
              lName: employeeData.lName,
              position: employeeData.position,
              image: employeeData.image,
              description: employeeData.description,
              instagram: employeeData.instagram,
              facebook: employeeData.facebook,
            };
            this.newEmployeeForm.setValue({
              fName: employeeData.fName,
              lName: employeeData.lName,
              position: employeeData.position,
              image: employeeData.image,
              description: employeeData.description,
              instagram: employeeData.instagram,
              facebook: employeeData.facebook,
            });
          }))
        : (this.mode = 'create');
    });
  }

  closeDialog() {
    this.dialogRef.close();
  }

  async onAddEmployee(form: FormGroup) {
    this.newEmployeeForm.disable();
    let response;
    switch (this.mode) {
      case 'create':
        this.snackBar.open('Adding employee to database!', '', {
          duration: 3000,
        });
        response = await this.postService.createEmployee(form.value);
        break;
      case 'edit':
        this.snackBar.open('Editing employee in database!', '', {
          duration: 3000,
        });
        response = await this.postService.updateEmployee(
          this.empId,
          form.value
        );
        break;
    }
    //after employee is added after closing the dialog in passing 1. the response containing the message and id received
    //from post service and 2. the form cobtaining all the info the user entered over to the about us component.
    this.dialogRef.close({ response: response, form: this.newEmployeeForm });
  }

  onImagePicked(event: Event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.newEmployeeForm.patchValue({ image: file });
    //checks wether value patched is valid
    this.newEmployeeForm.get('image').updateValueAndValidity();
    const reader = new FileReader();
    reader.onload = () => {
      this.imagePreview = reader.result as string;
    };
    reader.readAsDataURL(file);
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }
}
