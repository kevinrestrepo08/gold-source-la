import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Employee } from './about-us/employee.model';
import { Subject } from 'rxjs';
import { Product } from './home/new-product-form/product.model';

@Injectable({ providedIn: 'root' })
export class PostsService {
  private employees: Employee[] = [];
  private employeesUpdated = new Subject<{ employees: Employee[] }>();

  private products: Product[] = [];
  private productsUpdated = new Subject<{ products: Product[] }>();

  private shoppingCart: Product[] = [];
  private shoppingCartUpdated = new Subject<{ shoppingCart: Product[] }>();

  constructor(private http: HttpClient) {}

  createEmployee(employeeData: any) {
    const postData = new FormData();
    postData.append('fName', employeeData.fName),
      postData.append('lName', employeeData.lName),
      postData.append('position', employeeData.position),
      postData.append('image', employeeData.image, employeeData.fName),
      postData.append('description', employeeData.description),
      postData.append('instagram', employeeData.instagram),
      postData.append('facebook', employeeData.facebook);
    return new Promise((resolve, reject) => {
      this.http
        .post<{ message: string; employee: Employee }>(
          'http://localhost:3000/api/posts/addEmployee',
          postData
        )
        .subscribe((response) => {
          const employee: Employee = {
            id: response.employee.id,
            fName: employeeData.fName,
            lName: employeeData.lName,
            position: employeeData.position,
            image: response.employee.image,
            description: employeeData.description,
            instagram: employeeData.instagram,
            facebook: employeeData.facebook,
          };
          //passing the result message and id to the front end components
          resolve({
            message: response,
          });
        });
    });
  }

  updateEmployee(empId: string, employeeData: any) {
    const postData = new FormData();

    postData.append('id', empId),
      postData.append('fName', employeeData.fName),
      postData.append('lName', employeeData.lName),
      postData.append('position', employeeData.position),
      postData.append('description', employeeData.description),
      postData.append('instagram', employeeData.instagram),
      postData.append('facebook', employeeData.facebook);

    if (typeof employeeData.image == 'object') {
      postData.append('image', employeeData.image, employeeData.fName);
    } else {
      postData.append('image', employeeData.image);
    }

    return new Promise((resolve, reject) => {
      this.http
        .put('http://localhost:3000/api/posts/' + empId, postData)
        .subscribe((response) => {
          resolve({
            message: response,
          });
        });
    });
  }

  getEmployee(id: string) {
    return this.http.get<{
      _id: string;
      fName: string;
      lName: string;
      position: string;
      image: string;
      description: string;
      instagram: string;
      facebook: string;
    }>('http://localhost:3000/api/posts/' + id);
  }

  getAllEmployees() {
    this.http
      .get<{ message: string; employees: any }>(
        'http://localhost:3000/api/posts'
      )
      .pipe(
        map((employeeData) => {
          return {
            employees: employeeData.employees.map((employee: any) => {
              return {
                id: employee._id,
                fName: employee.fName,
                lName: employee.lName,
                image: employee.image,
                position: employee.position,
                description: employee.description,
                instagram: employee.instagram,
                facebook: employee.facebook,
              };
            }),
          };
        })
      )
      .subscribe((transformedPostData) => {
        this.employees = transformedPostData.employees;
        this.employeesUpdated.next({
          employees: [...this.employees],
        });
      });
  }

  getAllEmployeesUpdateListener() {
    return this.employeesUpdated.asObservable();
  }

  deleteEmployee(employee: Employee) {
    //remove urlpath from image to prevent coors error then re add in backend to delete
    let removeURLFromImg = employee.image.replace(
      'http://localhost:3000/images/',
      ''
    );
    return this.http.delete(
      'http://localhost:3000/api/posts/' + employee.id + '/' + removeURLFromImg
    );
  }

  createProduct(newProductData: any) {
    let productData = new FormData();
    productData.append('category', newProductData.category),
      productData.append('gender', newProductData.gender),
      // productData.append('images', newProductData.image),
      productData.append('length', newProductData.length),
      productData.append('price', newProductData.price),
      productData.append('productName', newProductData.productName),
      productData.append('stock', newProductData.stock),
      productData.append('weight', newProductData.weight),
      newProductData.images.forEach((element: any) => {
        productData.append('images', element);
      });
    return new Promise((resolve, reject) => {
      this.http
        .post<{ message: string; product: Product; images: string[] }>(
          'http://localhost:3000/api/products/addNewProduct',
          productData
        )
        .subscribe((response) => {
          resolve({
            message: response,
          });
        });
    });
  }

  getAllProducts() {
    this.http
      .get<{ message: string; products: any }>(
        'http://localhost:3000/api/products'
      )
      .pipe(
        map((productData) => {
          return {
            products: productData.products.map((product: any) => {
              return {
                id: product._id,
                productName: product.productName,
                category: product.category,
                gender: product.gender,
                images: product.images,
                length: product.length,
                weight: product.weight,
                price: product.price,
                stock: product.stock,
              };
            }),
          };
        })
      )
      .subscribe((transformedData) => {
        this.products = transformedData.products;
        this.productsUpdated.next({
          products: [...this.products],
        });
      });
  }

  deleteProduct(product: Product) {
    var [id, images] = [product.id, product.images];
    let imagesToDelete = [];
    for (let i = 0; i < images.length; i++) {
      imagesToDelete.push(
        images[i].replace('http://localhost:3000/images/products/', '')
      );
    }
    return this.http.delete(
      'http://localhost:3000/api/products/' + id + '/' + imagesToDelete
    );
  }

  getProduct(productId: string) {
    return this.http.get<{
      _id: string;
      productName: string;
      category: string;
      gender: string;
      images: Array<string>;
      length: number;
      weight: number;
      price: number;
      stock: number;
    }>('http://localhost:3000/api/products/' + productId);
  }

  updateProduct(productId: string, productData: any) {
    const postData = new FormData();

    postData.append('id', productId),
      postData.append('productName', productData.productName),
      postData.append('category', productData.category),
      postData.append('gender', productData.gender),
      postData.append('length', productData.length),
      postData.append('weight', productData.weight),
      postData.append('stock', productData.stock),
      postData.append('price', productData.price);

    if (typeof productData.images == 'object') {
      postData.append('images', productData.images);
    } else {
      postData.append('images', productData.images);
    }

    return new Promise((resolve, reject) => {
      this.http
        .put('http://localhost:3000/api/products/' + productId, postData)
        .subscribe((response) => {
          resolve({
            message: response,
          });
        });
    });
  }

  getAllProductsUpdateListener() {
    return this.productsUpdated.asObservable();
  }

  addToShoppingCart(product: Product) {
    this.shoppingCart.push(product);
    this.shoppingCartUpdated.next({ shoppingCart: [...this.shoppingCart] });
  }

  getShoppingCartListener() {
    return this.shoppingCartUpdated.asObservable();
  }
}
