import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { PostsService } from '../posts.service';
import { Employee } from './employee.model';
import { NewEmployeeFormComponent } from './new-employee-form/new-employee-form.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss'],
  animations: [
    // the fade-in/fade-out animation.
    trigger('FadeAnimation', [
      // the "in" style determines the "resting" state of the element when it is visible.
      state('in', style({ opacity: 1 })),

      // fade in when created. this could also be written as transition('void => *')
      transition(':enter', [style({ opacity: 0 }), animate(200)]),

      // fade out when destroyed. this could also be written as transition('void => *')
      transition(':leave', animate(400, style({ opacity: 0 }))),
    ]),
  ],
})
export class AboutUsComponent implements OnInit {
  userIsAuthenticated = false;
  private authStatusSub: Subscription;
  private employeesSub: Subscription;
  userId: string;
  team: Employee[];
  mode = 'create';

  skeleton = true;

  constructor(
    private authService: AuthService,
    private postService: PostsService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.userIsAuthenticated = this.authService.getIsAuth();
    this.authStatusSub = this.authService
      .getAuthStatusListener()
      .subscribe((isAuthenticated) => {
        this.userIsAuthenticated = isAuthenticated;
        this.userId = this.authService.getUserId();
      });

    this.postService.getAllEmployees();
    this.userId = this.authService.getUserId();
    this.employeesSub = this.postService
      .getAllEmployeesUpdateListener()
      .subscribe((employeeData: { employees: Employee[] }) => {
        this.team = employeeData.employees;
        this.skeleton = false;
      });
  }

  ngOnDestroy() {
    this.authStatusSub.unsubscribe();
    this.employeesSub.unsubscribe();
  }

  openDialog() {
    const dialogRef = this.dialog.open(NewEmployeeFormComponent, {
      data: { mode: this.mode },
    });
    //here im receiving the data from the new employee dialog component so that i can update the ui
    //with form data and id receivd from backend
    dialogRef.afterClosed().subscribe((result) => {
      // Remove query params
      this.router.navigate(['/about-us'], {
        queryParams: {
          empId: null,
        },
        queryParamsHandling: 'merge',
      });
      try {
        if (this.mode === 'create') {
          this.team.push({
            id: result.response.message.employee.id,
            fName: result.form.get('fName').value,
            lName: result.form.get('lName').value,
            image: result.response.message.employee.image,
            position: result.form.get('position').value,
            description: result.form.get('description').value,
            instagram: result.form.get('instagram').value,
            facebook: result.form.get('facebook').value,
          });
        }
        if (this.mode === 'edit') {
          let index = this.team.findIndex(
            (element) => element.id == result.response.message.employee.id
          );
          this.team[index] = {
            id: result.response.message.employee.id,
            fName: result.form.get('fName').value,
            lName: result.form.get('lName').value,
            image: result.response.message.employee.image,
            position: result.form.get('position').value,
            description: result.form.get('description').value,
            instagram: result.form.get('instagram').value,
            facebook: result.form.get('facebook').value,
          };
        }
        let resultEmoji;
        result.response.message == 'Employee added successfully'
          ? (resultEmoji = '👍🏽')
          : (resultEmoji = '👎🏽');
        this.snackBar.open(result.response.message.message + ' 👍🏽', '', {
          duration: 2000,
        });
      } catch (err) {
        this.snackBar.open('Adding employee canceled! 🙅🏽', '', {
          duration: 3000,
        });
      }
    });
  }

  onDeleteEmployee(member: Employee) {
    this.postService.deleteEmployee(member).subscribe(() => {
      const index = this.team.findIndex((element) => element.id == member.id);
      if (index > -1) {
        this.team.splice(index, 1);
        this.snackBar.open('deletion successful! 👍🏽', '', {
          duration: 3000,
        });
      } else {
        this.snackBar.open('something went wrong! 🤷🏽', '', {
          duration: 3000,
        });
      }
    });
  }

  changeMode(input: string) {
    this.mode = input;
  }
}
