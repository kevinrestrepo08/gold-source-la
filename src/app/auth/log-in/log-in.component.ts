import { Component, HostListener, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.scss'],
})
export class LogInComponent implements OnInit {
  screenHeight!: number;
  screenWidth!: number;
  logInForm!: FormGroup;
  signUpForm!: FormGroup;

  constructor(public authService: AuthService) {
    this.onResize();
  }

  ngOnInit(): void {
    this.logInForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [
        Validators.required,
        Validators.minLength(8),
      ]),
    });

    this.signUpForm = new FormGroup({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [
        Validators.required,
        Validators.minLength(8),
      ]),
      // fName: new FormControl(null, [Validators.required]),
      // lName: new FormControl(null, [Validators.required]),
      // description: new FormControl(null, [Validators.required]),
      // instaLink: new FormControl(null, [
      //   Validators.pattern('https://www.instagram.com/(.+)'),
      // ]),
      // facebookLink: new FormControl(null, [
      //   Validators.pattern('https://www.facebook.com/(.+)'),
      // ]),
    });
  }

  onLogIn(form: FormGroup) {
    if (form.invalid) {
      return;
    }
    this.authService.adminLogIn(form.value.email, form.value.password);
  }

  onSignUp(form: FormGroup) {
    if (form.invalid) {
      return;
    }
    this.authService.createAdmin(form.value.email, form.value.password);
  }

  //adjust the height of the log in card depending on the view
  @HostListener('window:resize', ['$event'])
  onResize() {
    this.screenHeight = window.innerHeight;
    this.screenWidth = window.innerWidth;
    this.screenWidth > this.screenHeight
      ? document
          .querySelector('.card')
          ?.setAttribute('style', 'transform: translate(0px, 150px);')
      : document
          .querySelector('.card')
          ?.setAttribute('style', 'transform: translate(0px, 35px);');
  }
}
