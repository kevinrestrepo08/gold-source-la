const mongoose = require("mongoose");

const employeeSchema = mongoose.Schema({
  fName: { type: String, required: true },
  lName: { type: String, required: true },
  position: { type: String, required: true },
  image: { type: String, required: true },
  description: { type: String, required: true },
  instagram: { type: String, required: false },
  facebook: { type: String, required: false },
});

module.exports = mongoose.model("Employee", employeeSchema);
