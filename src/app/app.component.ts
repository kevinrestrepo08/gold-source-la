import { Component, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from './auth/auth.service';
import { Product } from './home/new-product-form/product.model';
import { PostsService } from './posts.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  shoppingCart: Product[] = [];
  shoppingCartSub: Subscription;

  length = 0;

  title = 'gold-source-la';
  showShoppingCart = false;

  constructor(
    private authService: AuthService,
    private postService: PostsService
  ) {}

  ngOnInit() {
    this.authService.autoAuthUser();

    this.shoppingCartSub = this.postService
      .getShoppingCartListener()
      .subscribe((productData: { shoppingCart: Product[] }) => {
        this.shoppingCart = productData.shoppingCart;
      });
  }

  navItems = [
    {
      page: 'Home',
      route: 'home',
    },
    {
      page: 'About Us',
      route: 'about-us',
    },
    {
      page: 'Log In',
      route: 'log-in',
    },
  ];

  toggleShoppingCart() {
    this.showShoppingCart = !this.showShoppingCart;
  }
}
