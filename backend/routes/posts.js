const express = require("express");
const multer = require("multer");

const checkAuth = require("../middleware/check-auth");

const Employee = require("../models/employee");

const router = express.Router();

const axios = require("axios");
const FormData = require("form-data");
// include node fs module
const fs = require("fs");
const path = require("path");

const mime_type_map = {
  "image/png": "png",
  "image/jpeg": "jpg",
  "image/jpg": "jpg",
};
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const isValid = mime_type_map[file.mimetype];
    let error = new Error("invalid mime type");
    if (isValid) {
      error = null;
    }
    cb(null, "backend/images");
  },
  filename: (req, file, cb) => {
    const name = file.originalname.toLocaleLowerCase().split(" ").join("-");
    const ext = mime_type_map[file.mimetype];
    cb(null, name + "-" + Date.now() + "." + ext);
  },
});

router.post(
  "/addEmployee",
  checkAuth,
  multer({ storage: storage }).single("image"),
  (req, res, next) => {
    const url = req.protocol + "://" + req.get("host");
    removeImgBG("backend/images/" + req.file.filename);
    const employee = new Employee({
      fName: req.body.fName,
      lName: req.body.lName,
      position: req.body.position,
      image: url + "/images/" + req.file.filename,
      description: req.body.description,
      instagram: req.body.instagram,
      facebook: req.body.facebook,
    });
    employee
      .save()
      .then((createdEmployee) => {
        res.status(201).json({
          //passing the message and the id of the employee that was added to the front end.
          //this isnt authentication so passing id has no security risks
          message: "Employee added successfully",
          employee: {
            ...createdEmployee,
            id: createdEmployee._id,
            image: createdEmployee.image,
          },
        });
      })
      .catch((error) => {
        res.status(500).json({
          message: "Adding employee failed",
        });
      });
  }
);

router.get("", (req, res, next) => {
  const employeeQuery = Employee.find();
  let fetchedEmployees;
  employeeQuery
    .then((documents) => {
      fetchedEmployees = documents;
    })
    .then(() => {
      res.status(200).json({
        message: "Employees fetched successfully!",
        employees: fetchedEmployees,
      });
    })
    .catch((error) => {
      res.status(500).json({
        message: "Fetching employees failed!",
      });
    });
});

router.get("/:id", (req, res, next) => {
  Employee.findById(req.params.id)
    .then((post) => {
      if (post) {
        res.status(200).json(post);
      } else {
        res.status(404).json({ message: "Employee not found!" });
      }
    })
    .catch((error) => {
      res.status(500).json({
        message: "Fetching employee failed!",
      });
    });
});

router.put(
  "/:id",
  checkAuth,
  multer({ storage: storage }).single("image"),
  (req, res, next) => {
    let imagePath = req.body.image;
    if (req.file) {
      const url = req.protocol + "://" + req.get("host");
      imagePath = url + "/images/" + req.file.filename;
      removeImgBG(imagePath.replace(url, "backend"));
      //if user is uploading a new img file, then delete the old one from the project storage
      let previousImg;
      Employee.findById(req.params.id).then((post) => {
        previousImg = post.image.replace(
          "http://localhost:3000/images",
          "backend/images"
        );
        try {
          fs.unlink(previousImg, (err) => {
            if (err) {
              console.log("error in update route", err);
            } else {
              console.log("file deleted");
            }
          });
        } catch (error) {}
      });
    }
    const employee = new Employee({
      _id: req.body.id,
      fName: req.body.fName,
      lName: req.body.lName,
      position: req.body.position,
      image: imagePath,
      description: req.body.description,
      instagram: req.body.instagram,
      facebook: req.body.facebook,
    });
    Employee.updateOne({ _id: req.params.id }, employee)
      .then((result) => {
        if (result.modifiedCount > 0) {
          res.status(200).json({
            message: "Update successful!",
            employee: {
              ...employee,
              id: employee._id,
              image: employee.image,
            },
          });
        } else {
          res.status(401).json({ message: "Not authorized!" });
        }
      })
      .catch((error) => {
        res.status(500).json({
          message: "Couldn't udpate employee!",
        });
      });
  }
);

router.delete("/:id/:imgName", checkAuth, (req, res, next) => {
  Employee.deleteOne({ _id: req.params.id })
    .then((result) => {
      if (result.deletedCount > 0) {
        res.status(200).json({ message: "deletion successful" });
        fs.unlink("backend/images/" + req.params.imgName, (err) => {
          if (err) {
            console.log("error in delete route", err);
          } else {
            console.log("file deleted");
          }
        });
      } else {
        res.status(401).json({ message: "not authorized" });
      }
    })
    .catch((error) => {
      res.status(500).json({
        message: "Deleting employee failed!",
      });
    });
});

function removeImgBG(pathToImg) {
  //start of making img transparent
  const formData = new FormData();
  formData.append("size", "auto");
  formData.append(
    "image_file",
    fs.createReadStream(pathToImg),
    path.basename(pathToImg)
  );

  axios({
    method: "post",
    url: "https://api.remove.bg/v1.0/removebg",
    data: formData,
    responseType: "arraybuffer",
    headers: {
      ...formData.getHeaders(),
      "X-Api-Key": "BBZEvWqhS8eLa4T9rv9BQE2p",
    },
    encoding: null,
  })
    .then((response) => {
      if (response.status != 200)
        return console.error("Error:", response.status, response.statusText);
      fs.writeFileSync(
        "backend/images/" + pathToImg.replace("backend/images/", ""),
        response.data
      );
    })
    .catch((error) => {
      return console.error("Request failed:", error);
    });

  //end of making img transparent
}

module.exports = router;
